// Service (data handling) - exports EmployeeService which uses angularfire librarie's to interact with Firebase DB
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Employee } from '../models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private dbPath = '/employees';

  // Sets a list binding via Firebase' FireList
  employeesRef: AngularFireList<Employee>;

  constructor(private db: AngularFireDatabase) {
    this.employeesRef = db.list(this.dbPath);
  }

  // Pulls every record from the Employees DB
  getAll(): AngularFireList<Employee> {
    return this.employeesRef;
  }

  //Uses the push() method to add new items on the list.
  create(employee: Employee): any {
    return this.employeesRef.push(employee);
  }

  //Uses the update() method to update existing items.
  update(key: string, value: any): Promise<void> {
    return this.employeesRef.update(key, value);
  }

  //Uses the remove() method to remove data at the list item's location.
  delete(key: string): Promise<void> {
    return this.employeesRef.remove(key);
  }

  //By omiting the key parameter from .remove() it deletes the entire list.
  deleteAll(): Promise<void> {
    return this.employeesRef.remove();
  }
}
