import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';


@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  // Decorators set to mark list as an input and refreshList as an output
  @Input() employee?: Employee;
  @Output() refreshList:

  //Emit custom events or asynchronously, and register handlers for those events by subscribing to the instance.
  EventEmitter<any> = new EventEmitter();

  //Sets the 'current employee' with it's values as undefined.
  currentEmployee: Employee = {
    id_employee: '',
    name: '',
    picture: '',
    phone_number: '',
    email: '',
    hired_date: '',
    manager_id: '',
  };
  message = '';

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.message = '';
  }

  // Refreshes the list on changes.
  ngOnChanges(): void {
    this.message = '';
    this.currentEmployee = { ...this.employee };
  }

  // Updates selected employee from the list with its old a new values.
  updateEmployee(): void {
    // Grabs current data on the employee and updates it back.
    const data = {
      id_employee: this.currentEmployee.id_employee,
      name: this.currentEmployee.name,
      picture: this.currentEmployee.picture,
      phone_number: this.currentEmployee.phone_number,
      email: this.currentEmployee.email,
      hired_date: this.currentEmployee.hired_date,
      manager_id: this.currentEmployee.manager_id,
    };

    // If key is obtained use it to use update() method on correct employee. Insert 'data'. Use promises to secure confirmation of correct implementation.
    if (this.currentEmployee.key) {
      this.employeeService.update(this.currentEmployee.key, data)
        .then(() => this.message = 'The employee was updated successfully!')
        .catch(err => console.log(err));
    }
  }

  // Deletes selected employee.
  deleteEmployee(): void {
    // If key is obtained use it to use delete() method on chosen employee. Use promises to secure confirmation of correct implementation.
    if (this.currentEmployee.key) {
      this.employeeService.delete(this.currentEmployee.key)
        .then(() => {
          this.refreshList.emit();
          this.message = 'The employee was updated successfully!';
        })
        .catch(err => console.log(err));
    }
  }


}
