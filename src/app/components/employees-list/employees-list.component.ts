import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';
import { Employee } from 'src/app/models/employee.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {
  // Initializes empty array of employees
  employees?: Employee[];

  // Initializes current employee's var
  currentEmployee?: Employee;

  // Sets required vars with values
  currentIndex = -1;
  name = "";


  constructor(private employeeService: EmployeeService) { }

  // Handle additional initialization task: Retrieve whole employee list
  ngOnInit(): void {
    this.retrieveEmployee();
  }

  //refreshes list in case of a deletion
  refreshList(): void {
    this.currentEmployee = undefined;
    this.currentIndex = -1;
    this.retrieveEmployee();
  }

  // Unique keys are retrieved with RxJS tools for all employees for better data manipulation
  // Use snapshotChanges().map() to store the key
  retrieveEmployee(): void {
    this.employeeService.getAll().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ key: c.payload.key, ...c.payload.val() })
        )
      )
    ).subscribe(data => {
      this.employees = data;
    });
  }

  // Set current chosen employee as currentEmployee
  setActiveEmployee(employee: Employee, index: number): void {
    this.currentEmployee = employee;
    this.currentIndex = index;
  }

  // Reset current employee val
  setUnactiveEmployee(): void {
    this.currentEmployee = undefined;
    this.currentIndex = -1;
  }

  // Delete all employees from the list using method deleteAll() the refresh list.
  removeAllEmployees(): void {
    this.employeeService.deleteAll()
      .then(() => this.refreshList())
      .catch(err => console.log(err));
  }

  // Search functionality implemented with filter() method instead of angular's pipe.
  Search(){
    if(this.name != ""){
      this.employees = this.employees?.filter(res => {
      return res.name?.toLocaleLowerCase().match(this.name.toLocaleLowerCase())
    })
    } else if(this.name == ""){
      this.ngOnInit();
    }
  }
}
