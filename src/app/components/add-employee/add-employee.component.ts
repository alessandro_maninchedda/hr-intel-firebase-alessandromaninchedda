import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  // Initializes the imported class
  employee: Employee = new Employee();

  // Bool for testing purposes.
  submitted = false;

  constructor(private EmployeeService: EmployeeService) { }

  ngOnInit(): void {
  }

  // Utilizes service' method to save new employee's information
  saveEmployee(): void {
    this.EmployeeService.create(this.employee).then(() => {
      console.log('Created new item successfully!');
      this.submitted = true;
    });
  }

  // Bool resetted and class re-initalized to receive a new employee's information.
  newEmployee(): void {
    this.submitted = false;
    this.employee = new Employee();
  }

}
