// Data model definition class
export class Employee {
  key?: string | null;
  id_employee?: string;
  name?: string;
  picture?: string;
  phone_number?: string;
  email?: string;
  hired_date?: string;
  manager_id?: string;
}
