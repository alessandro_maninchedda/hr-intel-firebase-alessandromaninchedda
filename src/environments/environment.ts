// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBqkInE69pTvRU_N1jHEUvuoCCZrJp9S1E",
    authDomain: "alessandro-intel.firebaseapp.com",
    databaseURL: "https://alessandro-intel-default-rtdb.firebaseio.com",
    projectId: "alessandro-intel",
    storageBucket: "alessandro-intel.appspot.com",
    messagingSenderId: "1047779585284",
    appId: "1:1047779585284:web:dae7a5e4dfa32fc9c03451"
  } //configures information to connect with Firebase Project.
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
